from abc import ABC, abstractclassmethod

class Person(ABC):
    @abstractclassmethod

    def getFullName(self,user):
        return self.get_firstName(user) + " " + self.get_lastName(user)

    def addRequest(self):
        pass
    
    def checkRequest(self):
        pass

    def addUser():
        pass

class Request():
    def __init__(self, name, requester, dateRequested):
        self.name = name
        self.requester = requester
        self.dateRequested = dateRequested
        self.status = 'open'

    def updateRequest(self, name, requester, dateRequested, status):
        self.name = name
        self.requester = requester
        self.dateRequested = dateRequested
        self.status = status

    def set_status(self, status):
        self.status = status

    def closeRequest(self):
        self.status = 'closed'
        message = "Request" + self.name + "has been closed"
        return message
    
    def closeRequest(self):
        self.status = 'cancelled'
        message = "Request " + self.name + " has been cancelled"
        return message

class Employee(Person):

    # INITIALIZATION
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department

    # SETTERS
    def set_firstName(self, firstName):
        self._firstName = firstName

    def set_lastName(self, lastName):
        self._lastName = lastName

    def set_email(self, email):
        self._lastName = email

    def set_department(self, department):
        self._lastName = department

    # GETTERS
    def get_firstName(self):
        return self._firstName

    def get_lastName(self):
        return self._lastName
    
    def get_email(self):
        return self._email
    
    def get_department(self):
        return self._department
    
    # METHODS
    def getFullName(self):
        return super().getFullName(self)

    def addUser(self):
        pass
    
    def checkRequest(self):
        pass
    
    def addRequest(self):
        return "Request has been added"
    
    def login(self):
        return self._email + " has logged in"
    
    def logout(self):
        return self._email + " has logged out"
    
class TeamLead(Person):

    # INITIALIZATION
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
        self._members = []

    # SETTERS
    def set_firstName(self, firstName):
        self._firstName = firstName

    def set_lastName(self, lastName):
        self._lastName = lastName

    def set_email(self, email):
        self._lastName = email

    def set_department(self, department):
        self._lastName = department

    def set_members(self, members):
        self._members = members

    # GETTERS
    def get_firstName(self):
        return self._firstName

    def get_lastName(self):
        return self._lastName
    
    def get_email(self):
        return self._email
    
    def get_department(self):
        return self._department

    def get_members(self):
        return self._members
    
    # METHODS
    def getFullName(self):
        return super().getFullName(self)

    def addUser(self):
        pass
    
    def checkRequest(self):
        pass
    
    def addRequest(self):
        pass
    
    def login(self):
        return self._email + " has logged in"
    
    def logout(self):
        return self._email + " has logged out"
    
    def addMember(self, employee):
        self._members.append(employee)

class Admin(Person):

    # INITIALIZATION
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department

    # SETTERS
    def set_firstName(self, firstName):
        self._firstName = firstName

    def set_lastName(self, lastName):
        self._lastName = lastName

    def set_email(self, email):
        self._lastName = email

    def set_department(self, department):
        self._lastName = department

    # GETTERS
    def get_firstName(self):
        return self._firstName

    def get_lastName(self):
        return self._lastName
    
    def get_email(self):
        return self._email
    
    def get_department(self):
        return self._department
    
    # METHODS
    def getFullName(self):
        return super().getFullName(self)

    def addUser(self):
        return "User has been added"
    
    def checkRequest(self):
        pass
    
    def addRequest(self):
        pass
    
    def login(self):
        return self._email + " has logged in"
    
    def logout(self):
        return self._email + " has logged out"
    
# --------------------------- TEST CASES ------------------------------#

employee1 = Employee("John","Doe","djohn@mail.com","Marketing")
employee2 = Employee("Jane","Smith","sjane@mail.com","Marketing")
employee3 = Employee("Robert","Patterson","probert@mail.com","Sales")
employee4 = Employee("Brandon","Smith","sbrandon@mail.com","Sales")
admin1 = Admin("Monika","Justin","jmonika@mail.com","Marketing")
teamlead1 = TeamLead("Michael","Specter","smichael@mail.com","Sales")
req1 = Request("New hire orientation",teamlead1,"27-Jul-2021")
req2 = Request("Laptop repair",employee1,"1-Jul-2021")

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamlead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamlead1.addMember(employee3)
teamlead1.addMember(employee4)
for indiv_emp in teamlead1.get_members():
    print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"

req2.set_status("closed")
print(req2.closeRequest())